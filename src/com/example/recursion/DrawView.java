package com.example.recursion;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class DrawView extends View {
    Paint paint = new Paint();
    public double angle;
    public double x,y;
    
    public DrawView(Context context) {
        super(context);
        paint.setColor(Color.BLACK);
        angle = 90;
        //angle=0; rectangle
        x = 800;
        y = 200;
        
    }

    @Override
    public void onDraw(Canvas canvas) {
    	//Rectangle(canvas, 300);
    	turn(90);
    	Arrow(canvas, 700);
    }

    public void forward(double length, Canvas canvas)
    {
    	double lastX = this.x;
    	double lastY = this.y;
    	double newX = (double) (lastX + length * Math.cos(Math.toRadians(this.angle)));
    	double newY = (double) (lastY + length * Math.sin(Math.toRadians(this.angle)));
    	this.x = newX;
    	this.y = newY;
    	canvas.drawLine((float)lastX, (float)lastY, (float)newX,(float) newY, paint);
    }
    
    public void turn(double angle)
    {
    	double tmpAngle = this.angle + angle;
    	if(tmpAngle < 0){
    		tmpAngle = 360 + angle;
    	}
    	
    	tmpAngle = tmpAngle % 360;
    	
    	this.angle = tmpAngle; 
    } 
    
    public void Rectangle(Canvas canvas, double d){
    	
    	turn(45);
    	forward(d, canvas);
    	turn(90);
    	forward(d, canvas);
    	turn(90);
    	forward(d, canvas);
    	turn(90);
    	forward(d, canvas);
    	turn(90);
    	forward(d/2, canvas);
    	if(d>1) {
    		Rectangle(canvas, d*Math.sqrt(2)/2);
    	}
    }
    
    public void Arrow(Canvas canvas, double d){
    	if(d>1){
	    	forward(d, canvas);
	    	turn(135);
	    	forward(d/3, canvas);		turn(135); Arrow(canvas, d/5); turn(90); Arrow(canvas, d/5); turn(135);
	    	turn(180);
	    	forward(d/3, canvas);
	    	turn(270);
	    	forward(d/3, canvas);       turn(135); Arrow(canvas, d/5); turn(90); Arrow(canvas, d/5); turn(135);
	    	turn(180);
	    	forward(d/3, canvas);
	    	turn(135);
	    	forward(d, canvas);
	    	turn(180);
    	}
    }
}
